

# Change password login here (and here alone)
.LOGIN <- "EucFACE"
.PASS <- "sprayCO2"

# Load functions.
library(RCurl)
library(HIEv)
source("camfunctionsSUN_FACE.R")

# EucFACE username
setToken("RRKJZ1pquKB5TmB3Kw7v")

# Dataframe with manually selected positions for photos.
sampledfr <- read.csv("SUN_snapshot_key.csv", stringsAsFactors=FALSE)

# Sample all, makes JPGs in images/
f <- doCameraSampleTERN(sampledfr)

# Upload to HIEv
meta <- c("Snapshot for TERN : the CUP supersite",
          "- Photo taken daily at 9:00am (including DST if active)",
          "- View from EucFACE Ring 1",
          "- Camera settings (API): pan=-143, tilt=0.6, zoom=460",
          "Set up by Remko Duursma.",
          "")
HIEv:::uploadToHIEv(f, experiment_id=39, type='RAW', description=meta)

