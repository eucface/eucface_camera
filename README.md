# EucFACE code base : take daily canopy photos with the security cameras

This repository contains code to take photos 3 times a day, at 6 locations per EucFACE ring, and upload the images as zip files to the HIEv.

`SampleEucFACEsecurecam.R` : script to collect images from the security cameras at preset locations / zoom levels.
`SampleTERNsnapshot.R` : scipt to collect an image from Ring 1, every day at 9am, overlooking the TERN woodland.

Must be used in conjunction with Windows Scheduler:
- Three times daily (11am, 12pm and 1pm) to take photos (`SampleEucFACEsecurecam.R`)
- Once daily (11pm) to uploaded zipped images (one zip file containing all photos for the day for each ring) (`upload_todays_images.R`).

Can't find what you are looking for? Some old files were moved to `\archive`.

## Setup

The login and password for the security cameras are stored in `options.yml`, you have to make that file and place it in the root directory of this repository. It should look like,

```
login:
  - "mylogin"

password:
  - "mypassword"
```


## Contact 

Remko Duursma & Craig McNamara


## Online

See bitbucket.org/eucface for all scripts.
